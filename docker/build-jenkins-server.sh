#!/usr/bin/env bash
VERSION="2.263.1-lts-centos7"
TAG="mkwardakov/jenkinsci:2-$VERSION"
[[ "" == "$1" ]] && { echo "Gitlab API token s required as first parameter to the script"; exit 1; }
JENKINS_USER="${2:-jenkins}"
JENKINS_PASS="${3:-changeme}"

cat << EOF > setup.groovy
import jenkins.model.*
import hudson.security.*
def env = System.getenv()
def jenkins = Jenkins.getInstance()
if(!(jenkins.getSecurityRealm() instanceof HudsonPrivateSecurityRealm))
    jenkins.setSecurityRealm(new HudsonPrivateSecurityRealm(false))
if(!(jenkins.getAuthorizationStrategy() instanceof GlobalMatrixAuthorizationStrategy))
    jenkins.setAuthorizationStrategy(new GlobalMatrixAuthorizationStrategy())
def user = jenkins.getSecurityRealm().createAccount(env.JENKINS_USER, env.JENKINS_PASS)
user.save()
jenkins.getAuthorizationStrategy().add(Jenkins.ADMINISTER, env.JENKINS_USER)
jenkins.save()
EOF

cat << EOF > Dockerfile
FROM jenkins/jenkins:$VERSION
ENV CASC_JENKINS_CONFIG=/var/jenkins_home/jenkins.yaml \
    GITLAB_API_KEY="$1" \
    JAVA_OPTS="-Djenkins.install.runSetupWizard=false" \
    JENKINS_USER="$JENKINS_USER" \
    JENKINS_PASS="$JENKINS_PASS"
COPY setup.groovy /var/jenkins_home/init.groovy.d/setup.groovy
COPY jenkins.yaml /var/jenkins_home/jenkins.yaml
RUN jenkins-plugin-cli --plugins \
    configuration-as-code \
    credentials-binding \
    dark-theme \
    embeddable-build-status \
    gitlab-plugin \
    git-parameter \
    hudson-pview-plugin \
    hudson-wsclean-plugin \
    parameterized-scheduler \
    parameterized-trigger \
    pipeline-aggregator-view \
    pipeline-as-yaml \
    pipeline-stage-view \
    pollscm \
    rebuild \
    ssh \
    ssh-agent \
    timestamper \
    theme-manager \
    workflow-aggregator
EOF

docker build -t $TAG ./
#docker push $TAG
