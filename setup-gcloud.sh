#!/usr/bin/env bash
gcloud compute instances create-with-container jenkinsci --container-image mkwardakov/jenkinsci:2-2.263.1-lts-centos7 --tags jenkins-server --zone europe-north1-c
gcloud compute firewall-rules create allow-http8080 --allow tcp:8080 --target-tags jenkins-server
gcloud compute instances create-with-container linuxagent --container-image jenkins/inbound-agent --container-env JENKINS_URL=http://10.166.0.3:8080,JENKINS_SECRET=86c63088098c6862c086c6c32d3b06929baa32bf3bd4572319fee6574999583f,JENKINS_AGENT_NAME=linux-agent,JENKINS_AGENT_WORKDIR=/home/jenkins --tags jenkins-agent --zone europe-north1-c
