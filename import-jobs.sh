#!/usr/bin/env bash
set -e
set -x
# the script is supposed to be run from the root of the repo
[[ $(type virtualenv) == "" ]] && pip install -U virtualenv
# Based on https://jenkins-job-builder.readthedocs.io/en/latest/quick-start.html
virtualenv "./.venv" && source "./.venv/bin/activate"
[[ -d jenkins-job-builder ]] || {
    git clone https://opendev.org/jjb/jenkins-job-builder.git
}
pip install -r jenkins-job-builder/test-requirements.txt -e jenkins-job-builder/
pip install -r jenkins-job-builder/requirements.txt -e jenkins-job-builder/
cp jenkins_jobs.ini jenkins-job-builder/etc/
jenkins-jobs test *.yaml
jenkins-jobs --conf jenkins_jobs.ini update *.yaml
