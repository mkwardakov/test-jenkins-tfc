#!/usr/bin/env bash
set -e
set -x
docker run --detach --name localjenkins --publish 8080:8080 --publish 50000:50000 mkwardakov/jenkinsci:2-2.263.1-lts-centos7
# to run the agent, check web interface for node secret
# docker run --init jenkins/inbound-agent -url http://localhost:8080 $SECRET linux-agent
