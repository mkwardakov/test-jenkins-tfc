# Repository description

This repository is IaC approach sample for Jenkins.
Goals:
1. Build docker image with pre-configured Jenkins server.
2. Implement Terraform setup in GCE based on image from step #1 and official docker images of Jenkins slaves.
3. Run Jenkins Job Builder job vs the setup on every commit to the repository

## Docker image Build

The image is derived from [official Jenkins image](https://hub.docker.com/_/jenkins). Changes made:
- Pre-configured user by groovy script
- Pre-configured server by [Configuration as Codes plugin](https://www.jenkins.io/projects/jcasc/)
- A bunch of plugins are pre-installed

To build the image, just run from `docker/` folder:
```
./build-jenkins-server.sh $GITLAB_API_TOKEN $JENKINS_USER $JENKINS_PASS
```
First parameter is mandatory, credentials could be omitted in favour of user `jenkins` and password `changeme`.

Please keep in mind that image will have secure API token to your Gitlab repo so do not make the image public.

To upload the image to [Docker hub](https://hub.docker.com), run `docker login` and uncomment last line in `build-jenkins-server.sh`.

## TBD: Cloud setup and Terraform provisioning

`setup-gcloud.sh` has some bootstrap commands for GCE.

## Jenkins Job Builder

File description:
- `setup-local.sh` runs pre-configured custom docker image with Jenkins server on localhost
- `jenkins_jobs.ini` configuration file for Jenkins Job Builder, points to localhost server
- `job1.yaml` yaml build description. Polls this git repo for changes and runs simple bash script
- `import-jobs.sh` Jenkins Job Builder script uploads yaml jobs to the server using `virtualenv`

Usage is pretty simple:
1. Make sure that configuration file points to your Jenkins server instance
2. Creat or change yaml with a job description
3. Run `import-jobs.sh`